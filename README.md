
# Ionic Test Helper

To simplify testing Ionic 2 components in bulk, having a test helper class is a must. 


This class was inspired by the excellent articles:

* [https://lathonez.com/2018/ionic-2-unit-testing/](https://lathonez.com/2018/ionic-2-unit-testing/)
* [https://www.dev6.com/Angular-Unit-Testing-Tips-n-Tricks](https://www.dev6.com/Angular-Unit-Testing-Tips-n-Tricks)

## Install

Install with [npm](https://www.npmjs.com/):

```sh
$ npm install --save-dev ionic-test-helper
```

## Usage

The test helper can be used to generate the TestBed, or a component focused compiler.

### Creating a TestBed

```typescript
const testBed = IonicTestHelper.configureTestingModule({
    // Contains optional indexes. 
    // imports: []
    // providers: []
    // declarations: []

    // And the mandatory index.
    component: [TestComponent],
});

testBed.compileComponents();
```

### Simple Component Test

The most basic usage of the simply provides the component to be tested:

```typescript
describe('TestComponent', () => {
    let component: TestComponent;
    let fixture: ComponentFixture<TestComponent>;
    let el: Element;
    let compilation: Compilation;

    beforeEach(async(() => {
        IonicTestHelper.beforeEachComponent({
            component: TestComponent
        }).then((compiled: Compilation) => {
            compilation = compiled;
            fixture = compiled.fixture;
            component = compiled.instance;
            el = compiled.element;
        });
    });
    
    afterEach(async() => {
        IonicTestHelper.afterEachComponent(compilation);
    });
    
    it('it should create the component.', (() => {
        expect(component).toBeTruthy();
    }));
});
```

### Advanced Component Test

A more complex usage includes all the additional imports, declarations and providers.
It should be noted that components under test should not be part of the declarations,
as they are automatically added.

```typescript
describe('TestComponent', () => {
    let component: TestComponent;
    let fixture: ComponentFixture<TestComponent>;
    let compilation: Compilation;

    beforeEach(async(() => {
        IonicTestHelper.beforeEachComponent({
            imports: [
                TranslateModule
            ],
            providers: [
                { provide: TranslateService, useClass: TranslateServiceMock },
            ],
            declarations: [ 
                DependentComponent
            ],
            component: TestComponent
        }).then((compiled: Compilation) => {
            compilation = compiled;
            fixture = compiled.fixture;
            component = compiled.instance;
        });
    }));

    afterEach(async() => {
        IonicTestHelper.afterEachComponent(compiliation);
    });

    it('it should create the component.', (() => {
        expect(component).toBeTruthy();
    }));
});
```

### Component Test using Router

```typescript
describe('TestComponent', () => {
    let component: TestComponent;
    let fixture: ComponentFixture<TestComponent>;
    let route: any;
    let router: Router;
    let location: Location; 
    let routedCompilation: RoutedCompilation;

    beforeEach(async(() => {
        IonicTestHelper.beforeEachRouteComponent({
            component: TestComponent
        }, { id: 34, title: 'Read Me' }).then((compiled: RoutedCompilation) => {
            routedCompilation = compiled;
            fixture = compiled.fixture;
            component = compiled.instance;
            route = compiled.route;
            router = compiled.router;
            location = compiled.location;
        });
    }));

    afterEach(() => {
        IonicTestHelper.afterEachRouteComponent(routedCompilation);
    });

    it('it should create the component.', (() => {
        expect(component).toBeTruthy();
    }));
});
```

Updating the parameters can be done using:

```typescript
route.params.next({ id: 1 });
``` 

### Firing an Event.

Simplify the firing of an event on a native element.

```typescript
IonicTestHelper.eventFire(fixture.debugElement.nativeElement, 'click');
```