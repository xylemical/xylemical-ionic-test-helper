
import {Compilation, IonicTestHelper, RoutedCompilation} from './ionic-test-helper';
import {Component, Input} from '@angular/core';
import {By} from '@angular/platform-browser';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {fakeAsync, tick} from '@angular/core/testing';

export class TestProvider {
    description = 'Test Provider'
}

@Component({
    selector: 'test',
    template: '<p class="description">{{ description }}</p>'
})
export class TestComponent {
    @Input()
    description = 'Test Component'
}

@Component({
    template: '<test [description]="description"></test>'
})
export class ParentComponent {
    description = 'Parent Component';

    constructor(test: TestProvider) {
        this.description = test.description;
    }
}

@Component({
    selector: 'routed-test',
    template: '<p class="description">{{ description }}</p>'
})
export class RoutedTestComponent {
    description = 'Overridable Description';

    constructor(route: ActivatedRoute, router: Router) {
        route.data.subscribe((value: any) => {
            this.description = value.description;
        });
    }
}

describe('IonicTestHelper', () => {
    it('should provide a valid testbed.', () => {
        const TestBed = IonicTestHelper.configureTestingModule({
            component: TestComponent
        });

        const fixture = TestBed.createComponent(TestComponent);

        const de = fixture.debugElement.query(By.css('p.description'));

        fixture.detectChanges();
        const p = de.nativeElement;
        const description = 'Test Component';
        expect(p.textContent).toEqual(description);
    });

    it('should provide a valid component.', (done) => {
        IonicTestHelper.beforeEachComponent({
            component: TestComponent,
        }).then((compiled: Compilation) => {
            expect(compiled.instance instanceof TestComponent).toBeTruthy();

            IonicTestHelper.afterEachComponent(compiled);
            done();
        });
    });

    it('should handle additional definitions.', (done) => {
        IonicTestHelper.beforeEachComponent({
            declarations: [TestComponent],
            providers: [TestProvider],
            component: ParentComponent
        }).then((compiled: Compilation) => {
            expect(compiled.instance instanceof ParentComponent).toBeTruthy();

            compiled.fixture.detectChanges();
            const de = compiled.fixture.debugElement.query(By.css('p.description'));

            const p = de.nativeElement;
            const description = 'Test Provider';
            expect(p.textContent).toEqual(description);

            IonicTestHelper.afterEachComponent(compiled);
            done();
        });
    });

    it('should provide a valid component with route data.', fakeAsync(() => {
        const data = {
            description: 'Test Description'
        };

        return IonicTestHelper.beforeEachRouteComponent({
            component: RoutedTestComponent
        }, data).then((compiled: RoutedCompilation) => {
            expect(compiled.instance instanceof RoutedTestComponent).toBeTruthy();

            compiled.fixture.detectChanges();
            tick();

            const de = compiled.fixture.debugElement.query(By.css('p.description'));

            const p = de.nativeElement;
            expect(p.textContent).toEqual(data.description);

            IonicTestHelper.afterEachRouteComponent(compiled);
        });
    }));
});
