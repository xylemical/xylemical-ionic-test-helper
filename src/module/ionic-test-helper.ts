import {TestBed} from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
    App, Config, DeepLinker, DomController, Form, IonicModule, Keyboard, MenuController, NavController,
    Platform
} from 'ionic-angular';
import {ConfigMock, PlatformMock} from 'ionic-mocks';
import {ActivatedRoute} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {Subject} from 'rxjs/Subject';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {ErrorHandler, Injectable} from '@angular/core';

export class Options {
    imports?: any;
    providers?: any;
    declarations?: any;
    component: any;
}

export class Compilation {
    fixture: any;
    nativeElement: any;
    instance: any;
}

export class RoutedCompilation extends Compilation {
    route: any;
}

export function platformMockFactory() {
  return PlatformMock.instance();
}

export function configMockFactory() {
    return ConfigMock.instance();
}

@Injectable()
export class ErrorHandlerService implements ErrorHandler {

    constructor() {}

    handleError(error: any): void {
        this.processError(error);
        throw error;
    }

    public processError(error: any) {
        console.log(error);
    }

}

export class IonicTestHelper {

    /**
     * Provide simplified testing of a component used as a route component.
     *
     * @param options
     * @param params
     */
    public static beforeEachRouteComponent(options: Options, params: any = {}): Promise<RoutedCompilation> {
        // Create a mock route.
        const mockRoute: any = {
            snapshot: {},
            parent: { params: new BehaviorSubject<any>({})},
            params: new BehaviorSubject<any>(params),
            data: new BehaviorSubject<any>(params),
            queryParams: new BehaviorSubject<any>({}),
        };

        const imports = options.imports || [];
        const providers = options.providers || [];

        imports.push(RouterTestingModule.withRoutes([]));
        providers.push(
            { provide: ActivatedRoute, useValue: mockRoute }
        );

        const testBed = IonicTestHelper.configureTestingModule({
            ...options,
            imports,
            providers
        });

        return testBed.compileComponents().then(() => {
            const component = options.component;
            const fixture: any = TestBed.createComponent(component);
            return {
                fixture: fixture,
                nativeElement: fixture.nativeElement,
                instance: fixture.debugElement.componentInstance,
                route: mockRoute,
            };
        });
    }

    /**
     * Perform proper cleanup after the test completes.
     *
     * @param fixture
     */
    public static afterEachRouteComponent(fixture: RoutedCompilation) {
        const route = fixture.route;
        route.parent.params.complete();
        route.params.complete();
        route.queryParams.complete();
    }

    /**
     * Creates the fixtures from the provided components.
     *
     * @param options
     */
    public static beforeEachComponent(options: Options): Promise<Compilation> {
        return IonicTestHelper.configureTestingModule(options)
            .compileComponents()
            .then(() => {
                const component = options.component;
                const fixture: any = TestBed.createComponent(component);
                return {
                    fixture: fixture,
                    nativeElement: fixture.nativeElement,
                    instance: fixture.debugElement.componentInstance,
                };
            });
    }

    /**
     * Perform cleanup after the test completion.
     *
     * @param fixture
     */
    public static afterEachComponent(fixture: Compilation) {
        fixture.fixture.destroy();
    }

    /**
     * Configure the testing module for Ionic.
     *
     * @param options
     */
    public static configureTestingModule(options: Options): typeof TestBed {
        const imports = options.imports || [];
        const providers = options.providers || [];
        const declarations = options.declarations || [];
        const component = options.component;

        return TestBed.configureTestingModule({
            declarations: [
                ...declarations,
                component,
            ],
            providers: [
                App, Form, Keyboard, DomController, MenuController, NavController,
                {provide: Platform, useFactory: platformMockFactory},
                {provide: Config, useFactory: configMockFactory},
                {provide: DeepLinker, useFactory: configMockFactory},
                {provide: ErrorHandler, useClass: ErrorHandlerService},
                ...providers
            ],
            imports: [
                FormsModule,
                IonicModule,
                ReactiveFormsModule,
                ...imports
            ],
        });
    }

    /**
     * Fire an event on an element.
     *
     * @param el
     *
     * @param etype
     */
    public static eventFire(el: any, etype: string): void {
        if (el.fireEvent) {
            el.fireEvent('on' + etype);
        } else {
            const evObj: any = document.createEvent('Events');
            evObj.initEvent(etype, true, false);
            el.dispatchEvent(evObj);
        }
    }
}
